# ***Code for reproducible research presentation***

# This contains all the code shown in the presentation slides.
# Install git, conda, and docker before running.

# Init local git repository
git init

# Use Bitbucket's RESTFUL API to create a new repository and link it
# Use your own user instead of "reproducible_research" here
curl -s --user reproducible_research https://api.bitbucket.org/1.0/repositories/ --data name=reproducible_research
git remote add origin https://reproducible_research@bitbucket.org/reproducible_research/reproducible_research.git

# Add README.md and push
echo "Tutorial on reproducible research" > README.md
git add README.md
git commit -m "Initial commit"
git push --set-upstream origin master

# Add conda channels
conda config --add channels bioconda
conda config --add channels r

# Create a Conda environment with all the tools we need
conda create --name my_conda_env snakemake=3.5.5 fastqc=0.11.5 r-ggplot2=2.1.0 r-rmarkdown=0.9.6

# Export the conda environment. This file can be then be used to recreate the environment
# on a different computer (if not using Docker for this)
conda env export -n my_conda_env > conda_env.yml

# Activate conda environment and run Snakemake workflow to see that everything works
source activate my_conda_env
snakemake results/SRR068303.pdf
ls results

# Add all files to the git repository and push
git add Snakefile Dockerfile conda_env.yml source/sra_report.Rmd
git commit -m "Added all files"
git push

# Build the Docker image. Note that this will make use of the Dockerfile in the current folder,
# but that it will do a git clone to get the Snakefile, conda_env.yml, and source/sra_report.RMD files.
docker build -t "reproducibleresearch/docker_conda" .

# Mount the current directory at /home/reproducible_research/results in the Docker container.
# This will run the command set as default in the Dockerfile (see last line in that file).
docker run --rm -v $PWD:/home/reproducible_research/results reproducibleresearch/docker_conda
ls

# You could also work with the Docker container interactively
#docker run --rm -it -v $PWD/results:/home/reproducible_research/results reproducibleresearch/docker_conda /bin/bash

# Lastly, the Docker image is pushed to Docker hub so that others can use it (after docker pull)
# This requires that you have registered as the user "reproducibleresearch" at hub.docker.com.
docker push reproducibleresearch/docker_conda

# Some utility stuff for cleaning up
#docker rm $(docker ps --filter=status=exited --filter=status=created -q) #Remove all Docker containers
#docker rmi $(docker images -a --filter=dangling=true -q) ##Remove all Docker images
