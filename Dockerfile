FROM rocker/verse
MAINTAINER "Rasmus Agren" rasmus.agren@scilifelab.se

RUN apt-get update && apt-get -y dist-upgrade
RUN apt-get install -y --no-install-recommends bzip2

WORKDIR /home
ENV LC_ALL C
SHELL ["/bin/bash", "-c"]

RUN wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
RUN bash Miniconda3-latest-Linux-x86_64.sh -bf && rm Miniconda3-latest-Linux-x86_64.sh
ENV PATH="/root/miniconda3/bin:${PATH}"

# We don't need to install rmarkdown and gglot2 as we did before since they're included
# in the rocker/verse image. If we really care about pinning versions we still
# could though.
RUN conda config --add channels bioconda
RUN conda create -n my_conda_env snakemake=3.5.5 fastqc=0.11.5

RUN git clone https://reproducible_research@bitbucket.org/reproducible_research/reproducible_research.git

CMD source activate my_conda_env;cd reproducible_research;snakemake results/SRR068303.pdf
