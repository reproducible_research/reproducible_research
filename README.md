To reproduce all results in the presentation, choose one of the options below (A-E).

**A.** Clone this repo, install software manually, run scripts manually:

```
#!bash

$ git clone https://reproducible_research@bitbucket.org/reproducible_research/reproducible_research.git
$ # the rest is up to you...
```

**B.** Clone this repo, install software manually:

```
#!bash

$ git clone https://reproducible_research@bitbucket.org/reproducible_research/reproducible_research.git
$ cd reproducible_research
$ snakemake results/SRR068303.pdf
```

**C.** Clone this repo, use the conda environment file to facilitate software installation:

```
#!bash

$ git clone https://reproducible_research@bitbucket.org/reproducible_research/reproducible_research.git
$ cd reproducible_research
$ conda env create -f conda_env.yml
$ source activate my_conda_env
(my_conda_env)$ snakemake results/SRR068303.pdf

```
**D.** Clone this repo, build and run Docker image from Dockerfile:

```
#!bash

$ git clone https://reproducible_research@bitbucket.org/reproducible_research/reproducible_research.git
$ cd reproducible_research
$ docker build -t "my_docker_image" .
$ docker run --rm my_docker_image
```

**E.** Use the snapshot image of this repo + system + software from Docker hub:

```
#!bash

$ docker pull reproducibleresearch/docker_conda:paper_version
$ docker run --rm -it -v $PWD:/home/reproducible_research/results reproducibleresearch/docker_conda /bin/bash
root@27344a656192# cd reproducible_research
root@27344a656192# source activate my_conda_env
(my_conda_env)root@27344a656192# snakemake results/SRR068303.pdf
```
